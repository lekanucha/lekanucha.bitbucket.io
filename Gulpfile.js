var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var fileinclude = require('gulp-file-include');
var pkg = require('./package.json');


// Compiles SCSS files from /scss into /css
gulp.task('sass', function() {
   return gulp.src('sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Minify compiled CSS
gulp.task('minify-css', ['sass'], function() {
    return gulp.src(['css/*.css', '!css/*.min.css'])
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('fileinclude', function() {
  return gulp.src(['template/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./'))
    .pipe(browserSync.reload({
       stream: true
    }));
});


// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: '.'
        },
    })
})

//Watch task
gulp.task('default', ['browserSync', 'sass', 'minify-css', 'fileinclude'],function() {

    gulp.watch('sass/**/*.scss',['sass']);
    gulp.watch('css/*.css', ['minify-css']);
    gulp.watch(['template/**/*.html'], ['fileinclude']);
    gulp.watch(['js/**/*.js'], ['fileinclude']);
});


